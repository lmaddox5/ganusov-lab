#John Maddox
#Last updated: 1-19-2022
#Produces the individual viral shedding curves for Nonlytic Model
#R-Version: R-3.6.2

par(mfrow=c(1,1))

# Working Directory, Packages, and Functions
{
setwd("C:/Users/jlmad/Downloads/Haslam/ganusov-lab/Data")
library(nlme)
library(FME)
library(tibble)
library(Rmpfr)
library(xlsx)
source("functions.r")
}

#Nonlytic Model, ODE Model solving function and resids
#Additional Minor Functions
{  
  #RESIDS Function
  resids=function(fit, fix, dataset, modelname, modelcols, datacols, init.vector)
  {
    timepoints=dataset[,1]
    timename=names(dataset)[1]
    if(is.null(datacols)) datacols=2:(dim(dataset)[2]) # default to all x1...xn in the data
    #  if (is.null(init.conds)) init.conds=dataset[1, datacols] # take init conds from the data if necessary
    # It is useful for this particular data set as the numbers range from 1 to 3000, and the fits are not particularly great 
    soln=ode.solve(fit=fit,fix=fix,modelname=modelname, init.vector=init.vector, timepoints=timepoints, timename=timename)
    resids=as.vector(as.matrix(log10(soln[,modelcols])-(dataset[,datacols])))
    resids=na.omit(resids)
    resids=resids[!is.infinite(resids)]
  }
  
  #ODE MODEL SOLVER
  ode.solve=function(fit, fix, modelname, init.vector, timepoints, timename){ 
    allpars<-c(fit,fix)
    init.conds<-c() 
    for(i in 1:length(init.vector)){
      init.conds<-c(init.conds,allpars[init.vector[[i]]])
    }
    soln=as.data.frame(lsoda(y=init.conds, times=timepoints, func=modelname, parms=allpars))
    if (is.null(timename)) timename="day" 
    
    soln[,2]<-sapply(soln[,2],my.max1)
    names(soln)[1]=timename
    return(soln)
  }
  
  # NONLYTIC MODEL (OLD)
  virusGrowth<-function(time,state,parameters){
    with(as.list(c(state,parameters)),{
      dVs=Vs*r*ifelse(time<tstart,1,(1-e))*ifelse(time>ton,1,0)-((k*h^n)*Vs*(a0*exp(rho*time))^n/(1+h^n*(a0*exp(rho*time))^n))
      return(list(c(dVs)))
    })}
  # 
  # NONLYTIC MODEL
  virusGrowth<-function(time,state,parameters){
    with(as.list(c(state,parameters)),{
      dVs<-r*((Vs/(1+h^n*(A0*exp(rho*time))^n)))*ifelse(time<tstart,1,(1-e))-d*Vs
      return(list(c(dVs)))
    })}
  
  #Maximum and log transform function
  my.max1<-function(x) max(x,1)
  trans<-function(x) log10(x)
  
}

#Flu A Data and Parameters for Nonlytic model
{
  #Flu A data
  flua <- read.csv("FLU_A_FINAL.csv", header = TRUE)
  
  #Nonlytic model parameters
  flunpar <- read.csv("FLU_A_Parameters Nonlytic.csv")
  
}

############################################

#PLACEBO VOLUNTEER FINAL
{
  #removed (no shedding): 7, 21, 38, 41
  #removed for only having two valid points and late start of shedding: 58, 61, 67
  placebo<- c(4, 12, 18, 30, 33, 48, 52, 71, 77)
}

#PLACEBO FOR LOOP
for (patient in placebo){
  
  #Subsetting to only have necessary patient and their data from viral shedding file and parameter file
  Viralload<-subset(flua, subject==patient)
  Viralload<-Viralload[,c("days_since_start", "viral_titer")]
  viralpar <- subset(flunpar, subject ==patient)
  viralpar <- viralpar[,c("r", "d", "h", "Vs", "SSR")]
  
  #MODEL PARAMETERS
  {
    #FIXED PARAMETERS
    fix<-c(A0=0.01,n=55,ton=0.00,tstart=1.25,e=0.00, rho=0.38)
    
    #OLD PARAMETER GUESSES
    # fit<-c(r=6.12804, d=1.499054e+00, h=5.082476e+01, Vs=6.34523e-1)
    
    #FITTED PARAMETERS pulled from the file
    fit<-c(r=viralpar[,1], d=viralpar[,2], h=viralpar[,3], Vs = viralpar[,4])

    #OTHER NECESSARY PARAMETERS
    # fix<-c(t=8)
    init.vector<-c("Vs")
    lower<-rep(0,length(fit))
    upper<-rep(20000000000,length(fit))
  }

  # value of the immune response at time t
  eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))

  #PLOTS
  {
    #SOLVING ODE MODEL
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    
    #PLOTTING VIRAL SHEDDING POINTS
    plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
         ylab=expression("Viral Titer (TCID"[50]*"/ml)"),, yaxt = "n",
         ylim=c(0,8),  cex.lab = 1.3, cex.axis = 1.3, cex = 1.3)
    
    #AXIS
    axis(2, cex.axis=1.3, las = 1, at = seq(0, 8, 1), labels = c(expression(paste("10"^0)), expression(paste("10"^1)), expression(paste("10"^2)), expression(paste("10"^3)), expression(paste("10"^4)), expression(paste("10"^5)), expression(paste("10"^6)), expression(paste("10"^7)), expression(paste("10"^8))))
    
    #PLOTTING SOLUTION TO ODE MODEL
    lines(sol[,1],log10(sol[,2]),type="l",col=1,lwd=2.5)
    
    #LEGEND
    legend("topright", 
           title = paste("Control Flu A"),
           legend = c(paste("data (ID -", patient, ")"), "Nonlytic Model", paste("r = ", round(viralpar[,1], digits=1)), paste("h = ", round(viralpar[,3], digits=1)), as.expression(bquote('V'[0] == .(round(viralpar[,4],digits=6)))), bquote(delta == .(round(viralpar[,2],digits=1))), paste("SSR = ", round(viralpar[,5], digits=2))), 
           col = c("black", "black"), 
           bty = "b", 
           pch = c(1, NA, NA, NA, NA, NA, NA),
           lty = c(NA,1,NA, NA, NA, NA, NA),
           pt.cex = 1.2, 
           cex = 1.4, 
           lwd = c(NA, 2),
           text.col = c("black"),
           horiz = F )
  }

  #SEARCHING FOR A BETTER FIT
  {
  #fitting model to data
  modelfit1<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                    modelname=virusGrowth,
                    dataset=Viralload,
                    modelcols=c(2),datacols=c(2),
                    init.vector=init.vector,
                    method = c("Port")
                    #method = c("L-BFGS-B")
                    #method = c("BFGS")
  )
  
  #FINDING "BETTER FIT" PARAMETERS
  # fit<-coef(modelfit1)
  # 
  # #plotting the fit
  # {
  #   realsolfit<-ode.solve(coef(modelfit1),fix,virusGrowth,init.vector=init.vector,seq(0,10,0.01),"Days")
  #   #plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",ylab="Time (Days)",main=paste(namewhole))
  #   points(realsolfit[,1],log10(realsolfit[,2]),type="l",col="red",lwd=2,lty=2)
  # }
  }
  
  #MODEL INFO Calculations and Printing
  {
  #prints information over each loop
  print (coef(modelfit1))
  print(patient)
  print(summary(modelfit1))
  
  #ERROR Calculations
  {# s1<-summary(modelfit1)
  # s2<-s1$par[,2]
  # {
  #   if (patient == 4){
  #     subject <-(patient)
  #     rerror <-s2[1]
  #     derror<-s2[2]
  #     herror <-s2[3]
  #     Vserror<-s2[4]
  #     df<-data.frame(subject, rerror, derror, herror, Vserror)
  #   }
  # 
  #   if (patient != 4){
  #     df1 <- df %>% add_row(subject = patient,
  #                               rerror = s2[1],
  #                               derror = s2[2],
  #                               herror = s2[3],
  #                               Vserror = s2[4])
  #     df = df1
  #   }
  # }
}
  
  #CALCULATING AUC AND DOI
  {  
  timesteps<-sol[,1]
  titer<-sol[,2]

  temp<- titer
  auc_value_LOD_0=log10(AUC(timesteps, temp, interpolation=trapezoid)-10)
  
  auc_value_LOD_0.84=log10(AUC(timesteps, temp, interpolation=trapezoid)-((10^0.84)*10))
  if(!is.finite(auc_value_LOD_0.84)) auc_value_LOD_0.84 = 0
  
  auc_value_LOD_1.76=log10(AUC(timesteps, temp, interpolation=trapezoid)-((10^1.76)*10))
  if(!is.finite(auc_value_LOD_1.76)) auc_value_LOD_1.76 = 0
  
  auc_value_LOD_2.76=log10(AUC(timesteps, temp, interpolation=trapezoid)-((10^2.76)*10))
  if(!is.finite(auc_value_LOD_2.76)) auc_value_LOD_2.76 = 0

  if (patient == 4){
    subject <- patient
    auc_0 <- auc_value_LOD_0
    auc_0.84<-auc_value_LOD_0.84
    auc_1.76<-auc_value_LOD_1.76
    auc_2.76<-auc_value_LOD_2.76
    df<-data.frame(subject, auc_0, auc_0.84, auc_1.76, auc_2.76)
  }
  if (patient != 4){
    df1 <- df %>% add_row(subject = patient, auc_0 = auc_value_LOD_0, auc_0.84=auc_value_LOD_0.84, auc_1.76 = auc_value_LOD_1.76, auc_2.76 = auc_value_LOD_2.76)
    df = df1
    df
  }
  # 
  # temp<-titer
  # temp[temp <= 1] = 0
  # datframe<-data.frame(timesteps, temp)
  # 
  # lastmin = 0
  # for(i in datframe[,2]){
  #   if(i != 0){
  #     lastmin = i
  #   }}
  # doi_LOD_0 = datframe[,1][(which(temp == lastmin))]
  # if (lastmin == 0) {doi_LOD_0 = 0}
  # 
  # lastmin = 0
  # temp[temp <= 10^0.84] = 0
  # datframe<-data.frame(timesteps, temp)
  # for(i in datframe[,2]){
  #   if(i != 0){
  #     lastmin = i
  #   }}
  # doi_LOD_0.84 = datframe[,1][(which(temp == lastmin))]
  # if (lastmin == 0) {doi_LOD_0.84 = 0}
  # 
  # lastmin = 0
  # temp[temp <= 10^1.76] = 0
  # datframe<-data.frame(timesteps, temp)
  # for(i in datframe[,2]){
  #   if(i != 0){
  #     lastmin = i
  #   }}
  # doi_LOD_1.76 = datframe[,1][(which(temp == lastmin))]
  # if (lastmin == 0) {doi_LOD_1.76 = 0}
  # 
  # lastmin = 0
  # temp[temp <= 10^2.76] = 0
  # datframe<-data.frame(timesteps, temp)
  # for(i in datframe[,2]){
  #   if(i != 0){
  #     lastmin = i
  #   }}
  # doi_LOD_2.76 = datframe[,1][which(temp == lastmin)]
  # if (lastmin == 0) {doi_LOD_2.76 = 0}
  # 
  # if (patient == 4){
  #   subject <- patient
  #   doi_0 <- doi_LOD_0
  #   doi_0.84<-doi_LOD_0.84
  #   doi_1.76<-doi_LOD_1.76
  #   doi_2.76<-doi_LOD_2.76
  #   df<-data.frame(subject, doi_0, doi_0.84, doi_1.76, doi_2.76)
  # }
  # if (patient != 4){
  #   df1 <- df %>% add_row(subject = patient, doi_0 = doi_LOD_0, doi_0.84=doi_LOD_0.84, doi_1.76 = doi_LOD_1.76, doi_2.76 = doi_LOD_2.76)
  #   df = df1
  #   df
  # }
  }
  
  #CALCULATING SSR AND AIC
  {  # score <-AICc(modelfit1)
  # {
  #   if (patient == 4){
  #     subject <-(patient)
  #     aic <-score$AIC[1]
  #     delta <-score$delta[1]
  #     weight <-score$weights[1]
  #     dfplac<-data.frame(subject, aic, delta, weight)
  #   }
  # 
  #   if (patient != 4){
  #     df1 <- dfplac %>% add_row(subject = patient,
  #                               aic = score$AIC[1],
  #                               delta = score$delta[1],
  #                               weight = score$weights[1])
  #     dfplac = df1
  #   }
  # }
# write.csv(df, file = 'ssrestimates.csv')
# write.csv(dfplac, file = 'aicscoring.csv')

  
  
#CALCULATING SSR  
#   if (patient == 4){
#     subject <-(patient)
#     ssr <- sum(residuals(modelfit1)^2)
#     ssrc <- modelfit1$ssr
#     df<-data.frame(subject, ssr, ssrc)
#   }
# 
#   if (patient != 4){
#     df1 <- df %>% add_row(subject = patient, ssr = sum(residuals(modelfit1)^2), ssrc = modelfit1$ssr)
#     df = df1
#     df
#   }
# }
# # 
#  write.csv(df, file = 'ssrestimates.csv')
    
    # newssr<-modelfit1$ssr
    
    # if (patient == 4){
    #   subject <- patient
    #   r <- fit[1]
    #   d <- fit[2]
    #   h <- fit[3]
    #   Vs <- fit[4]
    #   SSR <- newssr
    #   df<-data.frame(subject, r, d, h, Vs, SSR)
    # }
    # else if (newssr < prevssr && patient != 4){
    #   df1 <- df %>% add_row(subject = patient, r = fit[1], d = fit[2], h = fit[3], Vs = fit[4], SSR = newssr)
    #   df = df1
    # }
    # 
}
  

}

}

############################################

#TREATED VOLUNTEER FINAL
{
  # treated<- c(1, 2, 3, 5, 6, 8, 9, 11, 14, 15, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 31, 34, 35, 36, 37, 39, 40, 44, 51, 53, 54, 55, 57, 59, 63, 64, 65, 70, 72, 75, 76)
  # removed: 10, 13, 16, 17, 32, 42, 43, 45, 46, 47, 49, 50, 56, 60, 62, 66, 68, 69, 73, 74, 78, 79, 80
  # error - difficult plot distribution: 20, 37
  # error - not enough points (on one or both slopes): 6, 19, 22, 24, 28, 57, 63, 65, 72, 76
  treated<- c(1, 2, 3, 5, 8, 9, 11, 14, 15, 23, 25, 26, 27, 29, 31, 34, 35, 36, 39, 40, 44, 51, 53, 54, 55, 59, 64, 70, 75)
}

#TREATED FOR LOOP
for(patient in treated){

  # Subsetting to only have necessary patient and their data from viral shedding file and parameter file
  Viralload<-subset(flua, subject==patient)
  Viralload<-Viralload[,c("days_since_start", "viral_titer")]
  viralpar <- subset(flunpar, subject == patient)
  viralpar <- viralpar[,c("r", "d", "h", "Vs", "SSR")]

  #PARAMETERS
  {
  #FIXED PARAMETERS
  fix<-c(A0=0.01,n=55,ton=0.00,tstart=1.25,e=0.00, rho=0.38)
  
  #OTHER PARAMETERS
  #fit<-c(r=19.1, d=5.1355, h=59.5375924, Vs=1.643E-3)
  #fit<-c(r=31.3784941, d=27.6824421, h=52.1603565, Vs=0.1373857)
  #51.3784941 46.6824421 52.1603565  0.1373857 1.1253
  
  #FITTED PARAMETERS pulled from a file
  fit<-c(r=viralpar[,1], d=viralpar[,2], h=viralpar[,3], Vs = viralpar[,4])

  #OTHER NECESSARY PARAMETERS
  #fix<-c(t=8)
  init.vector<-c("Vs")
  lower<-rep(0,length(fit))
  upper<-rep(20000000000,length(fit))
}

  # value of the immune response at time t
  eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))

#PLOTS
{
  #SOLVING THE ODE MODEL
  sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
  
  #PLOTTING THE VIRAL SHEDDING POINTS
  plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
       ylab=expression("Viral Titer (TCID"[50]*"/ml)"), yaxt = "n",
       ylim=c(0,8),  cex.lab = 1.3, cex.axis = 1.3, cex = 1.3)
  
  #AXIS
  axis(2, cex.axis=1.3, las = 1, at = seq(0, 8, 1), labels = c(expression(paste("10"^0)), expression(paste("10"^1)), expression(paste("10"^2)), expression(paste("10"^3)), expression(paste("10"^4)), expression(paste("10"^5)), expression(paste("10"^6)), expression(paste("10"^7)), expression(paste("10"^8))))
  
  #PLOTTING THE SOLUTION TO THE ODE MODEL
  lines(sol[,1],log10(sol[,2]),type="l",col=1,lwd=2.5)
  
  #LEGEND
  legend("topright", 
         title = paste("Treated Flu A"),
         legend = c(paste("data (ID -", patient, ")"), "Nonlytic Model", paste("r = ", round(viralpar[,1], digits=1)), paste("h = ", round(viralpar[,3], digits=1)), as.expression(bquote('V'[0] == .(round(viralpar[,4],digits=6)))), bquote(delta == .(round(viralpar[,2],digits=1))), paste("SSR = ", round(viralpar[,5], digits=2))), 
         col = c("black", "black"), 
         bty = "b", 
         pch = c(1, NA, NA, NA, NA, NA, NA),
         lty = c(NA,1,NA, NA, NA, NA, NA),
         pt.cex = 1.2, 
         cex = 1.4, 
         lwd = c(NA, 2),
         text.col = c("black"),
         horiz = F )
}

#SEARCHING FOR A BETTER FIT
{
modelfit2<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                  modelname=virusGrowth,
                  dataset=Viralload,
                  modelcols=c(2),datacols=c(2),
                  init.vector=init.vector,
                  method = c("Port")
                  #method = c("L-BFGS-B")
                  #method = c("BFGS")
)

# print(coef(modelfit2))
# fit<-coef(modelfit2)
# 
# realsolfit<-ode.solve(coef(modelfit2),fix,virusGrowth,init.vector=init.vector,seq(0,10,0.01),"Days")
# #plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",ylab="Time (Days)",main=paste(namewhole))
# points(realsolfit[,1],log10(realsolfit[,2]),type="l",col="red",lwd=2,lty=2)
}

#MODEL INFO Calculations and Printing
{
#prints information over each loop
print(patient)
print(summary(modelfit2))

#ERROR CALCULATIONS
{
# s1<-summary(modelfit2)
# s2<-s1$par[,2]

# {
#   if (patient == 1){
#     subject <-(patient)
#     rerror <-s2[1]
#     derror<-s2[2]
#     herror <-s2[3]
#     Vserror<-s2[4]
#     dftreat<-data.frame(subject, rerror, derror, herror, Vserror)
#   }
# 
#   if (patient != 4){
#     df1 <- df %>% add_row(subject = patient,
#                                rerror = s2[1],
#                                derror = s2[2],
#                                herror = s2[3],
#                                Vserror = s2[4])
#     df = df1
#   }
# }

# ssr <- sum(residuals(modelfit2)^2)

#write.xlsx(dftreat, file = 'errorNON2.xlsx')
}

#AUC and DOI CALCULATIONS
{
timesteps<-sol[,1]
titer<-sol[,2]

temp<- titer
auc_value_LOD_0=log10(AUC(timesteps, temp, interpolation=trapezoid)-10)

auc_value_LOD_0.84=log10(AUC(timesteps, temp, interpolation=trapezoid)-((10^0.84)*10))
if(!is.finite(auc_value_LOD_0.84)) auc_value_LOD_0.84 = 0

auc_value_LOD_1.76=log10(AUC(timesteps, temp, interpolation=trapezoid)-((10^1.76)*10))
if(!is.finite(auc_value_LOD_1.76)) auc_value_LOD_1.76 = 0

auc_value_LOD_2.76=log10(AUC(timesteps, temp, interpolation=trapezoid)-((10^2.76)*10))
if(!is.finite(auc_value_LOD_2.76)) auc_value_LOD_2.76 = 0

if (patient == 4){
  subject <- patient
  auc_0 <- auc_value_LOD_0
  auc_0.84<-auc_value_LOD_0.84
  auc_1.76<-auc_value_LOD_1.76
  auc_2.76<-auc_value_LOD_2.76
  df<-data.frame(subject, auc_0, auc_0.84, auc_1.76, auc_2.76)
}
if (patient != 4){
  df1 <- df %>% add_row(subject = patient, auc_0 = auc_value_LOD_0, auc_0.84=auc_value_LOD_0.84, auc_1.76 = auc_value_LOD_1.76, auc_2.76 = auc_value_LOD_2.76)
  df = df1
  df
}

# temp<-titer
# temp[temp <= 1] = 0
# datframe<-data.frame(timesteps, temp)
# 
# lastmin = 0
# for(i in datframe[,2]){
#   if(i != 0){
#     lastmin = i
#   }}
# doi_LOD_0 = datframe[,1][(which(temp == lastmin))]
# if (lastmin == 0) {doi_LOD_0 = 0}
# 
# lastmin = 0
# temp[temp <= 10^0.84] = 0
# datframe<-data.frame(timesteps, temp)
# for(i in datframe[,2]){
#   if(i != 0){
#     lastmin = i
#   }}
# doi_LOD_0.84 = datframe[,1][(which(temp == lastmin))]
# if (lastmin == 0) {doi_LOD_0.84 = 0}
# 
# lastmin = 0
# temp[temp <= 10^1.76] = 0
# datframe<-data.frame(timesteps, temp)
# for(i in datframe[,2]){
#   if(i != 0){
#     lastmin = i
#   }}
# doi_LOD_1.76 = datframe[,1][(which(temp == lastmin))]
# if (lastmin == 0) {doi_LOD_1.76 = 0}
# 
# lastmin = 0
# temp[temp <= 10^2.76] = 0
# datframe<-data.frame(timesteps, temp)
# for(i in datframe[,2]){
#   if(i != 0){
#     lastmin = i
#   }}
# doi_LOD_2.76 = datframe[,1][which(temp == lastmin)]
# if (lastmin == 0) {doi_LOD_2.76 = 0}
# 
# if (patient == 4){
#   subject <- patient
#   doi_0 <- doi_LOD_0
#   doi_0.84<-doi_LOD_0.84
#   doi_1.76<-doi_LOD_1.76
#   doi_2.76<-doi_LOD_2.76
#   df<-data.frame(subject, doi_0, doi_0.84, doi_1.76, doi_2.76)
# }
# if (patient != 4){
#   df1 <- df %>% add_row(subject = patient, doi_0 = doi_LOD_0, doi_0.84=doi_LOD_0.84, doi_1.76 = doi_LOD_1.76, doi_2.76 = doi_LOD_2.76)
#   df = df1
#   df
# }
  

  # write.xlsx(df, file = 'NONA_DOI_ALL_LODs.xlsx')
}

#AIC and SSR CALCULATIONS
{
# newssr<-modelfit2$ssr
# print(newssr)

# score <-AICc(modelfit2)

# if (patient == 1){
#   subject <- patient
#   r <- fit[1]
#   d <- fit[2]
#   h <- fit[3]
#   Vs <- fit[4]
#   SSR <- newssr
#   df<-data.frame(subject, r, d, h, Vs, SSR)
# }
# else if (newssr < prevssr && patient != 1){
#   df1 <- df %>% add_row(subject = patient, r = fit[1], d = fit[2], h = fit[3], Vs = fit[4], SSR = newssr)
#   df = df1
# }
# }
# {
  # if (patient == 1){
  #   subject <-(patient)
  #   aic <-score$AIC[1]
  #   delta <-score$delta[1]
  #   weight <-score$weights[1]
  #   dftreat<-data.frame(subject, aic, delta, weight)
  # }
  # 
  # if (patient != 1){
  #   df2 <- dftreat %>% add_row(subject = patient,
  #                              aic = score$AIC[1],
  #                              delta = score$delta[1],
  #                              weight = score$weights[1])
  #   dftreat = df2
  # }
# write.csv(dftreat, file = 'aicscoring.csv')


#CALCULATING SSR
# if (patient == 1){
#   subject <-(patient)
#   ssr <- sum(residuals(modelfit1)^2)
#   ssrc <- modelfit1$ssr
#   df<-data.frame(subject, ssr, ssrc)
# }
# 
# if (patient != 1){
#   df1 <- df %>% add_row(subject = patient, ssr = sum(residuals(modelfit1)^2), ssrc = modelfit1$ssr)
#   df = df1
#   df
#   }
# 
#write.csv(df, file = 'ssrestimates.csv')
}
}
}
write.xlsx(df, file = 'NONA_AUC_ALL_LODs.xlsx')
