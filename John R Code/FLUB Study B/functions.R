#AIC function

{
  AICc1 <- function(model1) {
    ssr<-model1$ssr
    npar<-length(model1$par)
    ndata<-length(model1$residuals)
    ndata*log(ssr/ndata)+2*npar+2*npar*(npar+1)/(ndata-npar-1)
  } 
  
  
  AICc <- function(...) {
    models <- list(...) 
    nmod <- length(models)
    aic <- c()
    for(i in 1:nmod) {
      aic <- c(aic,AICc1(models[[i]]))
    }
    
    minA <- min(aic)
    delta <- aic-minA
    weights <- c()
    for(i in 1:nmod) {
      weights <- c(weights,exp(-(aic[[i]]-minA)/2))
    }
    
    weights <- weights/sum(weights)
    
    list(AIC=aic,delta=delta,weights=weights)
    
  }
}

#Fig Letter Function
{
  put.fig.letter <- function(label, location="topleft", x=NULL, y=NULL, 
                             offset=c(0, 0), ...) {
    if(length(label) > 1) {
      warning("length(label) > 1, using label[1]")
    }
    if(is.null(x) | is.null(y)) {
      coords <- switch(location,
                       topleft = c(0.015,0.98),
                       topcenter = c(0.5525,0.98),
                       topright = c(0.985, 0.98),
                       bottomleft = c(0.015, 0.02), 
                       bottomcenter = c(0.5525, 0.02), 
                       bottomright = c(0.985, 0.02),
                       c(0.015, 0.98) )
    } else {
      coords <- c(x,y)
    }
    this.x <- grconvertX(coords[1] + offset[1], from="nfc", to="user")
    this.y <- grconvertY(coords[2] + offset[2], from="nfc", to="user")
    text(labels=label[1], x=this.x, y=this.y, xpd=T, ...)
  }
}