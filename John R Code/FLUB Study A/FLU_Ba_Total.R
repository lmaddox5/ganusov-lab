#John Maddox
#Last updated: 1-15-2022
#Produces the individual shedding curves from all three models on one graph per individual
#R-Version: R-3.6.2  

# setting working directory of where data are stored
setwd("C:/Users/jlmad/Downloads/Haslam/ganusov-lab/Data")
library(nlme)
library(FME)
library(xlsx)

{  # defining functions
  resids=function(fit, fix, dataset, modelname, modelcols, datacols, init.vector)
  {
    timepoints=dataset[,1]
    timename=names(dataset)[1]
    if(is.null(datacols)) datacols=2:(dim(dataset)[2]) # default to all x1...xn in the data
    #  if (is.null(init.conds)) init.conds=dataset[1, datacols] # take init conds from the data if necessary
    # It is useful for this particular data set as the numbers range from 1 to 3000, and the fits are not particularly great 
    soln=ode.solve(fit=fit,fix=fix,modelname=modelname, init.vector=init.vector, timepoints=timepoints, timename=timename)
    resids=as.vector(as.matrix(log10(soln[,modelcols])-(dataset[,datacols])))
    resids=na.omit(resids)
    resids=resids[!is.infinite(resids)]
  }
  
  ode.solve=function(fit, fix, modelname, init.vector, timepoints, timename){ 
    allpars<-c(fit,fix)
    init.conds<-c() 
    for(i in 1:length(init.vector)){
      init.conds<-c(init.conds,allpars[init.vector[[i]]])
    }
    soln=as.data.frame(lsoda(y=init.conds, times=timepoints, func=modelname, parms=allpars))
    if (is.null(timename)) timename="day" 
    
    soln[,2]<-sapply(soln[,2],my.max1)
    names(soln)[1]=timename
    return(soln)
  }
  
  
  ode.solveTCL=function(fit, fix, modelname, init.vector, timepoints, timename){ 
    allpars<-c(fit,fix)
    init.conds<-c() 
    for(i in 1:length(init.vector)){
      init.conds<-c(init.conds,allpars[init.vector[[i]]])
    }
    soln=as.data.frame(lsoda(y=init.conds, times=timepoints, func=modelname, parms=allpars))
    if (is.null(timename)) timename="day" 
    
    soln[,3]<-sapply(soln[,3],my.max1)
    names(soln)[1]=timename
    return(soln)
  }
  
  virusGrowthTCL<-function(time,state,parameters) 
  {with(as.list(c(state,parameters)),{
    dT<- -1*(beta)*TT*(Vs)
    dVs<-betaf*TT*(Vs)-d*(Vs)
    return(list(c(dT,dVs)))
  })}
  
  virusGrowthCyto<-function(time,state,parameters) 
  {with(as.list(c(time,state,parameters)),{
    t<-time
    dVs<-(r*Vs)-(k*Vs*(h^n)*(Ao*exp(rho*t))^n)/(1+(h^n)*(Ao*exp(rho*t))^n)
    return(list(c(dVs)))
  })}
  
  virusGrowthNon<-function(time,state,parameters){
    with(as.list(c(state,parameters)),{
      dVs=Vs*r*ifelse(time<tstart,1,(1-e))*ifelse(time>ton,1,0)-((k*h^n)*Vs*(a0*exp(rho*time))^n/(1+h^n*(a0*exp(rho*time))^n))
      return(list(c(dVs)))
    })}
  
  virusGrowthNon<-function(time,state,parameters){
    with(as.list(c(state,parameters)),{
      dVs<-ifelse(time>ton,1,0)*r*((Vs/(1+h^n*(A0*exp(rho*time))^n)))*ifelse(time<tstart,1,(1-e))-d*Vs
      return(list(c(dVs)))
    })}
}

par(mfrow=c(1,1))


# defining limit of detection
my.max1<-function(x) max(x,1)
trans<-function(x) log10(x)

{ 
  # data details
  fluba <- read.csv("FLU_B_study_A.csv", header = TRUE)
  flubaparCYT <-read.xlsx("FLU_Ba_Parameters_CYT.xlsx", 1)
  flubaparTCL <-read.xlsx("FLU_Ba_Parameters_TCL.xlsx", 1)
  flubaparNON <-read.xlsx("FLU_Ba_Parameters_NON.xlsx", 1)
}


# pdf("allfigs.pdf", width = 15, height = 12)
# par(mfrow=c(3,3), mar=c(4, 4, 2, 0.5), mgp = c(2.3, 0.7, 0))

placebo<-c(14, 16, 21, 28, 53, 65, 66)
# placebo<- c(48, 52, 77)
#placebo<- c(4, 12, 18, 30, 33, 48, 52, 67, 71, 77)

for (patient in placebo){
  
  # patient = 77
  
  # controls 
  Viralload<-subset(fluba, subject==patient)
  Viralload<-Viralload[,c("days_since_start", "viral_titer")]
  
  viralparTCL <- subset(flubaparTCL, subject == patient)
  viralparTCL <- viralparTCL[,c("beta", "f", "d", "Vs", "SSR", "AIC")]
  
  viralparNON <- subset(flubaparNON, subject == patient)
  viralparNON <- viralparNON[,c("r", "d", "h", "Vs", "SSR", "AIC")]
  
  viralparCYT <- subset(flubaparCYT, subject == patient)
  viralparCYT <- viralparCYT[,c("r", "k", "Vs", "h", "SSR", "AIC")]
  
  if(min(viralparTCL[,6], viralparNON[,6], viralparCYT[,6]) == viralparTCL[,6]){
    deltaTCL = 0
    deltaCYT = viralparCYT[,6]-viralparTCL[,6]
    deltaNON = viralparNON[,6]-viralparTCL[,6]
    weightTCL = exp(-0.5*deltaTCL)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightCYT = exp(-0.5*deltaCYT)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightNON = exp(-0.5*deltaNON)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
  }
  
  else if(min(viralparTCL[,6], viralparNON[,6], viralparCYT[,6]) == viralparNON[,6]){
    deltaTCL = viralparTCL[,6]-viralparNON[,6]
    deltaCYT = viralparCYT[,6]-viralparNON[,6]
    deltaNON = 0
    weightTCL = exp(-0.5*deltaTCL)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightCYT = exp(-0.5*deltaCYT)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightNON = exp(-0.5*deltaNON)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
  }
  
  else if(min(viralparTCL[,6], viralparNON[,6], viralparCYT[,6]) == viralparCYT[,6]){
    deltaTCL = viralparTCL[,6]-viralparCYT[,6]
    deltaCYT = 0
    deltaNON = viralparNON[,6]-viralparCYT[,6]
    weightTCL = exp(-0.5*deltaTCL)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightCYT = exp(-0.5*deltaCYT)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightNON = exp(-0.5*deltaNON)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
  }
  
  #plotting
  plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
       ylab=expression("Viral Titer (TCID"[50]*"/ml)"), yaxt = "n",
       ylim=c(0,14), xlim =c(0, 12), cex.lab = 1.3, cex.axis = 1.3, cex = 1.3)
  axis(2, las = 1, cex.axis = 1.3, at = seq(0, 14, 2), cex = 1.3, labels = c(expression(paste("10"^0)), expression(paste("10"^2)), expression(paste("10"^4)), expression(paste("10"^6)), expression(paste("10"^8)), expression(paste("10"^10)), expression(paste("10"^12)), expression(paste("10"^14))))
  
  
  
  {#TCL
    fit<-c(beta=viralparTCL[,1], betaf=viralparTCL[,2]*viralparTCL[,1], d=viralparTCL[,3], Vs=viralparTCL[,4])
    
    fix<-c(TT=1)
    init.vector<-c("TT","Vs")
    lower<-rep(0,length(fit))
    upper<-rep(1e7,length(fit))
    solTCL<-ode.solveTCL(fit,fix,virusGrowthTCL,init.vector,seq(0,20,0.02),"Days") 
    lines(solTCL[,1],log10(solTCL[,3]),lty=1,col=1,lwd=2)
    
    modelfitTCL<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                        modelname=virusGrowthTCL,
                        dataset=Viralload,
                        modelcols=c(2),datacols=c(2),
                        init.vector=init.vector,
                        method = c("Port")
                        #method = c("L-BFGS-B")
                        #method = c("BFGS")
    )
  }
  
  {#cyto
    fix<-c(t=8)
    init.vector<-c("Vs")
    lower<-rep(0,length(fit))
    upper<-rep(1e5,length(fit))
    
    # parameters for shedding in controls
    fix<-c(Ao=0.01, rho=0.38, n=55)
    #fit<-c(r=9.93, k=13.57, Vs=0.003172475, h=57.88)
    fit<-c(r=viralparCYT[,1], k=viralparCYT[,2], Vs=viralparCYT[,3], h=viralparCYT[,4])
    
    # value of the immune response at time t
    eval(substitute((h*Ao*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
    solCYT<-ode.solve(fit,fix,virusGrowthCyto,init.vector,seq(0,20,0.02),"Days") 
    lines(solCYT[,1],log10(solCYT[,2]),lty=2,col="red",lwd=2)
    
    modelfitCYT<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                        modelname=virusGrowthCyto,
                        dataset=Viralload,
                        modelcols=c(2),datacols=c(2),
                        init.vector=init.vector,
                        method = c("Port")
                        #method = c("L-BFGS-B")
                        #method = c("BFGS")
    )
  }
  
  {#non
    fix<-c(t=8)
    init.vector<-c("Vs")
    lower<-rep(0,length(fit))
    upper<-rep(20000000000,length(fit))
    
    # parameters for shedding in controls
    fit<-c(r=viralparNON[,1], d=viralparNON[,2], h=viralparNON[,3], Vs = viralparNON[,4])
    #fit<-c(r=1.811e+01, d=3.346e+00, h=6.697e+01, Vs=7.636e-07)
    fix<-c(A0=0.01,n=55,ton=0.00,tstart=1.25,e=0.00, rho=0.38)
    
    # value of the immune response at time t
    eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
    solNON<-ode.solve(fit,fix,virusGrowthNon,init.vector,seq(0,20,0.02),"Days") 
    lines(solNON[,1],log10(solNON[,2]),lty=3,col="blue",lwd=2)
    
    modelfitNON<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                        modelname=virusGrowthNon,
                        dataset=Viralload,
                        modelcols=c(2),datacols=c(2),
                        init.vector=init.vector,
                        method = c("Port")
                        #method = c("L-BFGS-B")
                        #method = c("BFGS")
    )
  }
  
  
  legend("topright",
         title = paste("Control Flu B Study A"),
         legend = c(paste("data (ID -", patient, ")"), paste("Simplified TCL Model (W =",round(weightTCL, digits=3),")"), paste("Cytolytic Model (W =", round(weightCYT, digits=3),")"), paste("Nonlytic Model (W =", round(weightNON, digits=3), ")")),
         #legend = c(paste("data (ID -", patient, ")"), paste("Simplified TCL Model"), paste("Cytolytic Model"), paste("Nonlytic Model")),
         col = c("black", "black", "red", "blue"),
         bty = "b",
         pch = c(1, NA, NA, NA),
         lty = c(NA , 1, 2, 3),
         pt.cex = 1.2,
         cex = 1.4,
         lwd = 1.7,
         text.col = c("black"),
         horiz = F )
}



treated<-c(15, 19, 22, 24, 43, 46, 54, 64, 68)
for (patient in treated){
  
  # patient = 25
  
  # controls 
  Viralload<-subset(fluba, subject==patient)
  Viralload<-Viralload[,c("days_since_start", "viral_titer", "drug_concentration")]
  
  viralparTCL <- subset(flubaparTCL, subject == patient)
  viralparTCL <- viralparTCL[,c("beta", "f", "d", "Vs", "SSR", "AIC")]
  
  viralparNON <- subset(flubaparNON, subject == patient)
  viralparNON <- viralparNON[,c("r", "d", "h", "Vs", "SSR", "AIC")]
  
  viralparCYT <- subset(flubaparCYT, subject == patient)
  viralparCYT <- viralparCYT[,c("r", "k", "Vs", "h", "SSR", "AIC")]
  
  if(min(viralparTCL[,6], viralparNON[,6], viralparCYT[,6]) == viralparTCL[,6]){
    deltaTCL = 0
    deltaCYT = viralparCYT[,6]-viralparTCL[,6]
    deltaNON = viralparNON[,6]-viralparTCL[,6]
    weightTCL = exp(-0.5*deltaTCL)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightCYT = exp(-0.5*deltaCYT)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightNON = exp(-0.5*deltaNON)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
  }
  
  else if(min(viralparTCL[,6], viralparNON[,6], viralparCYT[,6]) == viralparNON[,6]){
    deltaTCL = viralparTCL[,6]-viralparNON[,6]
    deltaCYT = viralparCYT[,6]-viralparNON[,6]
    deltaNON = 0
    weightTCL = exp(-0.5*deltaTCL)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightCYT = exp(-0.5*deltaCYT)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightNON = exp(-0.5*deltaNON)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
  }
  
  else if(min(viralparTCL[,6], viralparNON[,6], viralparCYT[,6]) == viralparCYT[,6]){
    deltaTCL = viralparTCL[,6]-viralparCYT[,6]
    deltaCYT = 0
    deltaNON = viralparNON[,6]-viralparCYT[,6]
    weightTCL = exp(-0.5*deltaTCL)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightCYT = exp(-0.5*deltaCYT)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
    weightNON = exp(-0.5*deltaNON)/(exp(-0.5*deltaTCL)+exp(-0.5*deltaNON)+exp(-0.5*deltaCYT))
  }
  
  
  #plotting
  plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
       ylab=expression("Viral Titer (TCID"[50]*"/ml)"), yaxt = "n",
       ylim=c(0,14), xlim =c(0, 12), cex.lab = 1.3, cex.axis = 1.3, cex = 1.3)
  axis(2, las = 1, cex.axis = 1.3, at = seq(0, 14, 2), cex = 1.3, labels = c(expression(paste("10"^0)), expression(paste("10"^2)), expression(paste("10"^4)), expression(paste("10"^6)), expression(paste("10"^8)), expression(paste("10"^10)), expression(paste("10"^12)), expression(paste("10"^14))))
  
  
  {#TCL
    fit<-c(beta=viralparTCL[,1], betaf=viralparTCL[,2]*viralparTCL[,1], d=viralparTCL[,3], Vs=viralparTCL[,4])
    
    fix<-c(TT=1)
    init.vector<-c("TT","Vs")
    lower<-rep(0,length(fit))
    upper<-rep(1e7,length(fit))
    solTCL<-ode.solveTCL(fit,fix,virusGrowthTCL,init.vector,seq(0,20,0.02),"Days") 
    points(solTCL[,1],log10(solTCL[,3]),type="l",col=1,lwd=2)
    
    modelfitTCL<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                        modelname=virusGrowthTCL,
                        dataset=Viralload,
                        modelcols=c(2),datacols=c(2),
                        init.vector=init.vector,
                        method = c("Port")
                        #method = c("L-BFGS-B")
                        #method = c("BFGS")
    )
  }
  
  {#cyto
    fix<-c(t=8)
    init.vector<-c("Vs")
    lower<-rep(0,length(fit))
    upper<-rep(1e5,length(fit))
    
    # parameters for shedding in controls
    fix<-c(Ao=0.01, rho=0.38, n=55)
    #fit<-c(r=9.93, k=13.57, Vs=0.003172475, h=57.88)
    fit<-c(r=viralparCYT[,1], k=viralparCYT[,2], Vs=viralparCYT[,3], h=viralparCYT[,4])
    
    # value of the immune response at time t
    eval(substitute((h*Ao*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
    solCYT<-ode.solve(fit,fix,virusGrowthCyto,init.vector,seq(0,20,0.02),"Days") 
    lines(solCYT[,1],log10(solCYT[,2]),lty=2,col="red",lwd=2)
    
    modelfitCYT<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                        modelname=virusGrowthCyto,
                        dataset=Viralload,
                        modelcols=c(2),datacols=c(2),
                        init.vector=init.vector,
                        method = c("Port")
                        #method = c("L-BFGS-B")
                        #method = c("BFGS")
    )
  }
  
  {#non
    fix<-c(t=8)
    init.vector<-c("Vs")
    lower<-rep(0,length(fit))
    upper<-rep(20000000000,length(fit))
    
    # parameters for shedding in controls
    fit<-c(r=viralparNON[,1], d=viralparNON[,2], h=viralparNON[,3], Vs = viralparNON[,4])
    #fit<-c(r=1.811e+01, d=3.346e+00, h=6.697e+01, Vs=7.636e-07)
    fix<-c(A0=0.01,n=55,ton=0.00,tstart=1.25,e=0.00, rho=0.38)
    
    # value of the immune response at time t
    eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
    solNON<-ode.solve(fit,fix,virusGrowthNon,init.vector,seq(0,20,0.02),"Days") 
    lines(solNON[,1],log10(solNON[,2]),lty=3,col="blue",lwd=2)
    
    modelfitNON<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                        modelname=virusGrowthNon,
                        dataset=Viralload,
                        modelcols=c(2),datacols=c(2),
                        init.vector=init.vector,
                        method = c("Port")
                        #method = c("L-BFGS-B")
                        #method = c("BFGS")
    )
  }
  
  legend("topright",
         title = paste("Treated Flu B Study A (", Viralload[1,3], ")"),
         legend = c(paste("data (ID -", patient, ")"), paste("Simplified TCL Model (W =", round(weightTCL, digits=3),")"), paste("Cytolytic Model (W =", round(weightCYT, digits=3),")"), paste("Nonlytic Model (W =", round(weightNON, digits=3), ")")),
         # legend = c(paste("data (ID -", patient, ")"), paste("Simplified TCL Model"), paste("Cytolytic Model"), paste("Nonlytic Model")),
         col = c("black", "black", "red", "blue"),
         bty = "b",
         pch = c(1, NA, NA, NA),
         lty = c(NA , 1, 2, 3),
         pt.cex = 1.2,
         cex = 1.4,
         lwd = 1.7,
         text.col = c("black"),
         horiz = F )
  
}
# dev.off()