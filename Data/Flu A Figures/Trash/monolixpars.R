setwd("C:/Users/jlmad/Downloads/Haslam/Research 2020 Influenza/John R Code/FLUA")

flucpar <- read.csv("FLU_A_Parameters Cyto.csv")
flunpar <- read.csv("FLU_A_Parameters Nonlytic.csv")
flutpar <- read.xlsx("FLU_A_Parameters TCL.xlsx", sheetIndex = 1)

placfluc <- subset(flucpar, drug_concentration == "Placebo")
placflun <- subset(flunpar, drug_concentration == "Placebo")
placflut <- subset(flutpar, drug_concentration == "Placebo")

#Cytolytic General estimates
placavrfluc <- mean(placfluc$r)
placavkfluc <- mean(placfluc$k)
placavVsfluc <- mean(placfluc$Vs)
placavhfluc <- mean(placfluc$h)

class <- "Placebo Averages"
rest <- placavrfluc
kest <- placavkfluc
Vsest <- placavVsfluc
hest <- placavhfluc
dfcyt<-data.frame(class, rest, kest, Vsest, hest)

placmedrfluc <- median(placfluc$r)
placmedkfluc <- median(placfluc$k)
placmedVsfluc <- median(placfluc$Vs)
placmedhfluc <- median(placfluc$h)

df1 <- dfcyt %>% add_row(class = "Placebo Median", rest = placmedrfluc, kest = placmedkfluc, Vsest = placmedVsfluc, hest = placmedhfluc)
dfcyt = df1

avrfluc <- mean(flucpar$r)
avkfluc <- mean(flucpar$k)
avVsfluc <- mean(flucpar$Vs)
avhfluc <- mean(flucpar$h)

df1 <- dfcyt %>% add_row(class = "Total Mean", rest = avrfluc, kest = avkfluc, Vsest = avVsfluc, hest = avhfluc)
dfcyt = df1

medrfluc <- median(flucpar$r)
medkfluc <- median(flucpar$k)
medVsfluc <- median(flucpar$Vs)
medhfluc <- median(flucpar$h)

df1 <- dfcyt %>% add_row(class = "Total Median", rest = medrfluc, kest = medkfluc, Vsest = medVsfluc, hest = medhfluc)
dfcyt = df1






#Nonlytic General estimates
placavrflun <- mean(placflun$r)
placavdflun <- mean(placflun$d)
placavVsflun <- mean(placflun$Vs)
placavhflun <- mean(placflun$h)

class <- "Placebo Averages"
rest <- placavrflun
dest <- placavdflun
Vsest <- placavVsflun
hest <- placavhflun
dfnon <- data.frame(class, rest, dest, Vsest, hest)

placmedrflun <- median(placflun$r)
placmeddflun <- median(placflun$d)
placmedVsflun <- median(placflun$Vs)
placmedhflun <- median(placflun$h)

df1 <- dfnon %>% add_row(class = "Placebo Median", rest = placmedrflun, dest = placmeddflun, Vsest = placmedVsflun, hest = placmedhflun)
dfnon = df1

avrflun <- mean(flunpar$r)
avdflun <- mean(flunpar$d)
avVsflun <- mean(flunpar$Vs)
avhflun <- mean(flunpar$h)

df1 <- dfnon %>% add_row(class = "Total Mean", rest = avrflun, dest = avdflun, Vsest = avVsflun, hest = avhflun)
dfnon = df1

medrflun <- median(flunpar$r)
meddflun <- median(flunpar$d)
medVsflun <- median(flunpar$Vs)
medhflun <- median(flunpar$h)

df1 <- dfnon %>% add_row(class = "Total Mean", rest = medrflun, dest = meddflun, Vsest = medVsflun, hest = medhflun)
dfnon = df1





#TCL General estimates
placavbetaflut <- mean(placflut$beta)
placavfflut <- mean(placflut$f)
placavdflut <- mean(placflut$d)
placavVsflut <- mean(placflut$Vs)

class <- "Placebo Averages"
betaest <- placavbetaflut
fest <- placavfflut
dest <- placavdflut
Vsest <- placavVsflut
dftcl <- data.frame(class, betaest, fest, dest, Vsest)

placmedbetaflut <- median(placflut$beta)
placmedfflut <- median(placflut$f)
placmeddflut <- median(placflut$d)
placmedVsflut <- median(placflut$Vs)

df1 <- dftcl %>% add_row(class = "Placebo Median", betaest = placmedbetaflut, fest = placmedfflut, dest = placmeddflut, Vsest = placmedVsflut)
dftcl = df1

avbetaflut <- mean(flutpar$beta, na.rm = TRUE)
avfflut <- mean(flutpar$f, na.rm = TRUE)
avdflut <- mean(flutpar$d, na.rm = TRUE)
avVsflut <- mean(flutpar$Vs, na.rm = TRUE)

df1 <- dftcl %>% add_row(class = "Total Mean", betaest = avbetaflut, fest = avfflut, dest = avdflut, Vsest = avVsflut)
dftcl = df1

medbetaflut <- median(flutpar$beta, na.rm = TRUE)
medfflut <- median(flutpar$f, na.rm = TRUE)
meddflut <- median(flutpar$d, na.rm = TRUE)
medVsflut <- median(flutpar$Vs, na.rm = TRUE)

df1 <- dftcl %>% add_row(class = "Total Median", betaest = medbetaflut, fest = medfflut, dest = meddflut, Vsest = medVsflut)
dftcl = df1