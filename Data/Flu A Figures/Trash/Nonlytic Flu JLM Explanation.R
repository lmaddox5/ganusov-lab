#John Maddox
#Last updated: 4-26-2021
#Produces the explanatory figure for Nonlytic Parameter manipulation
#R-Version: R-3.6.2  


# setting working directory of where data are stored
setwd("C:/Users/jlmad/Downloads/Haslam/Research 2020 Influenza/John R Code/FLUA")
library(nlme)
library(FME)

{  # defining functions
  resids=function(fit, fix, dataset, modelname, modelcols, datacols, init.vector)
  {
    timepoints=dataset[,1]
    timename=names(dataset)[1]
    if(is.null(datacols)) datacols=2:(dim(dataset)[2]) # default to all x1...xn in the data
    #  if (is.null(init.conds)) init.conds=dataset[1, datacols] # take init conds from the data if necessary
    # It is useful for this particular data set as the numbers range from 1 to 3000, and the fits are not particularly great 
    soln=ode.solve(fit=fit,fix=fix,modelname=modelname, init.vector=init.vector, timepoints=timepoints, timename=timename)
    resids=as.vector(as.matrix(log10(soln[,modelcols])-(dataset[,datacols])))
    resids=na.omit(resids)
    resids=resids[!is.infinite(resids)]
  }
  
  ode.solve=function(fit, fix, modelname, init.vector, timepoints, timename){ 
    allpars<-c(fit,fix)
    init.conds<-c() 
    for(i in 1:length(init.vector)){
      init.conds<-c(init.conds,allpars[init.vector[[i]]])
    }
    soln=as.data.frame(lsoda(y=init.conds, times=timepoints, func=modelname, parms=allpars))
    if (is.null(timename)) timename="day" 
    
    soln[,2]<-sapply(soln[,2],my.max1)
    names(soln)[1]=timename
    return(soln)
  }
  
  virusGrowth<-function(time,state,parameters){
    with(as.list(c(state,parameters)),{
      dVs=Vs*r*ifelse(time<tstart,1,(1-e))*ifelse(time>ton,1,0)-((k*h^n)*Vs*(a0*exp(rho*time))^n/(1+h^n*(a0*exp(rho*time))^n))
      return(list(c(dVs)))
    })}
  
  virusGrowth<-function(time,state,parameters){
    with(as.list(c(state,parameters)),{
      dVs<-ifelse(time>ton,1,0)*r*((Vs/(1+h^n*(A0*exp(rho*time))^n)))*ifelse(time<tstart,1,(1-e))-d*Vs
      return(list(c(dVs)))
    })}
}

{
  put.fig.letter <- function(label, location="topleft", x=NULL, y=NULL, 
                             offset=c(0, 0), ...) {
    if(length(label) > 1) {
      warning("length(label) > 1, using label[1]")
    }
    if(is.null(x) | is.null(y)) {
      coords <- switch(location,
                       topleft = c(0.015,0.98),
                       topcenter = c(0.5525,0.98),
                       topright = c(0.985, 0.98),
                       bottomleft = c(0.015, 0.02), 
                       bottomcenter = c(0.5525, 0.02), 
                       bottomright = c(0.985, 0.02),
                       c(0.015, 0.98) )
    } else {
      coords <- c(x,y)
    }
    this.x <- grconvertX(coords[1] + offset[1], from="nfc", to="user")
    this.y <- grconvertY(coords[2] + offset[2], from="nfc", to="user")
    text(labels=label[1], x=this.x, y=this.y, xpd=T, ...)
  }
}


namewhole = "Nonlytic Model"

# defining limit of detection
my.max1<-function(x) max(x,1)
trans<-function(x) log10(x)

{ 
  # data details
  flua <- read.csv("FLU_A_FINAL.csv", header = TRUE)
  
  #parameter file for Nonlytic model
  flunpar <- read.csv("FLU_A_Parameters Nonlytic.csv")
}

  placebo<- c(4, 12, 18, 30, 33, 48, 52, 71, 77)

  # controls
  Viralload<-subset(flua, subject==33)
  Viralload<-Viralload[,c("days_since_start", "viral_titer")]
  
  #parameter file being subset into individual patients
  viralpar <- subset(flunpar, subject ==33)
  viralpar <- viralpar[,c("r", "d", "h", "Vs")]
  
  fix<-c(t=8)
  init.vector<-c("Vs")
  lower<-rep(0,length(fit))
  upper<-rep(20000000000,length(fit))
  
  {
    # parameters for shedding in controls
    fix<-c(A0=0.0082,n=40,ton=0.00,tstart=1.25,e=0.00, rho=0.38)
    #fit<-c(r=viralpar[,1], d=viralpar[,2], rho=viralpar[,3])
    fit<-c(r=1.65E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-06)
    # Increase r: Shift up and raises right tail (Virus proliferation rate up) 
    # Decrease r: Shift down and lowers right tail (Virus proliferation rate down) 
      # Increase means more virus, so more viral shedding occurs as there is more virus present elongating total shedding time
      # Decrease means less virus, so less viral shedding occurs as there is less virus present shortening total shedding time
    # Increase d: Shift down and lowers right tail (Viral Proliferation Inhibition rate up) 
    # Decrease d: Raises right tail (Viral Killing rate down) 
      # Increase causes more virus proliferation inhibition shortening total shedding time
      # Decrease causes less virus proliferation inhibition shortening total shedding time
    # Increase rho: Scales graph down (Initial Viral Amount Up) 
    # Decrease rho: Scales graph up (Initial viral Amount Down)
      # Increase in initial viral amount allows more viral proliferation to occur shifting total viral shedding up
      # Decrease in initial viral amount allows less viral proliferation to occur shifting total viral shedding down
    
  }

  pdf("NonLyticFluAParInf.pdf", width = 11, height = 9) 
  
  # value of the immune response at time t
  eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
  
  par(mfrow=c(2,2), mar=c(4, 4, 2, 0.5), mgp = c(2.0, 0.7, 0))

  # plotting the data and model solutions given the parameter guesses
  {
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
         ylab=bquote(paste("Viral Titer")), yaxt = "n",
         ylim=c(0,8), cex.axis=1.2, cex.lab=1.2)
    axis(2, las =1, at = seq(0, 8, 2), labels = c(expression(paste("10"^0)), expression(paste("10"^2)), expression(paste("10"^4)), expression(paste("10"^6)), expression(paste("10"^8))))
    lines(sol[,1],log10(sol[,2]),type="l",col=1,lwd=2)
    fit<-c(r=1.75E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-06)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=2,col="red",lwd=2)
    fit<-c(r=1.55E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-06)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=3,col="blue",lwd=2)
    
    
    legend("topright", 
           title = "Virus Proliferation Influence",
           legend = c("Flu A ID-33", "Fitted r", "Increased r", "Decreased r"), 
           col = c("black", "black", "red", "blue"), 
           bty = "n", 
           pch = c(1, NA, NA, NA),
           lty = c(NA,1,2,3),
           pt.cex = 1.1, 
           cex = 1.1, 
           text.col = c("black", "black", "red", "blue"), 
           horiz = F )
    
    put.fig.letter('A', offset = c(0.02, -0.01), cex=2, font=2)
    legend("topleft", "Nonlytic Model", adj=0.15, bty = "n", cex = 1.1)
    text(6.1, 1.8, col = "black", "r = 16.5/day")
    text(5, 4, col = "red", "r = 17.5/day")
    text(3.7, 1, col = "blue", "r = 15.5/day")
  }



  #fitting model to data
  modelfit1<-modFit(f=resids,p=fit,fix=fix,lower=lower,upper=upper,
                    modelname=virusGrowth,
                    dataset=Viralload,
                    modelcols=c(2),datacols=c(2),
                    init.vector=init.vector,
                    method = c("Port")
                    #method = c("L-BFGS-B")
                    #method = c("BFGS")
  )
  
  fit<-c(r=1.65E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-06)
  eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
  
  # plotting the data and model solutions given the parameter guesses
  {
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
         ylab=bquote(paste("Viral Titer")), yaxt = "n",
         ylim=c(0,8), cex.axis=1.2, cex.lab=1.2)
    axis(2, las = 1, at = seq(0, 8, 2), labels = c(expression(paste("10"^0)), expression(paste("10"^2)), expression(paste("10"^4)), expression(paste("10"^6)), expression(paste("10"^8))))
    lines(sol[,1],log10(sol[,2]),type="l",col=1,lwd=2)
    fit<-c(r=1.65E+01, d=3.43E+00, h=6.16E+01, Vs=8.24E-06)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=2,col="red",lwd=2)
    fit<-c(r=1.65E+01, d=2.43E+00, h=6.16E+01, Vs=8.24E-06)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=3,col="blue",lwd=2)
    
    legend("topright", 
           title = "Viral Death Rate Influence",
           legend = c("Flu A ID-33", expression(paste("Fitted ", delta)), expression(paste("Increased ", delta)), expression(paste("Decreased ", delta))), 
           col = c("black", "black", "red", "blue"), 
           bty = "n", 
           pch = c(1, NA, NA, NA),
           lty = c(NA,1,2,3),
           pt.cex = 1.1, 
           cex = 1.1, 
           text.col = c("black", "black", "red", "blue"), 
           horiz = F )
    
    put.fig.letter('B', offset = c(0.02, -0.01), cex=2, font=2)
    legend("topleft", "Nonlytic Model", adj=0.15, bty = "n", cex = 1.1)
    text(6.5, 1.0, col = "black", expression(paste(delta, " = 2.93/day")))
    text(3.5, 0.7, col = "red", expression(paste(delta, " = 3.43/day")))
    text(5.3, 4, col = "blue",  expression(paste(delta, " = 2.43/day")))
  }
  
  fit<-c(r=1.65E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-06)
  eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
  
  # plotting the data and model solutions given the parameter guesses
  {
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
         ylab=bquote(paste("Viral Titer")), yaxt = "n",
         ylim=c(0,8),  cex.axis=1.2, cex.lab=1.2)
    axis(2, las = 1, at = seq(0, 8, 2), labels = c(expression(paste("10"^0)), expression(paste("10"^2)), expression(paste("10"^4)), expression(paste("10"^6)), expression(paste("10"^8))))
    lines(sol[,1],log10(sol[,2]),type="l",col=1,lwd=2)
    fit<-c(r=1.65E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-05)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=2,col="red",lwd=2)
    fit<-c(r=1.65E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-07)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=3,col="blue",lwd=2)
    
    legend("topright", 
           title = "Initial Virus Amount Influence",
           legend = c("Flu A ID-33", expression('Fitted'*' V'[0]), expression('Increased'*' V'[0]), expression('Decreased'*' V'[0])), 
           col = c("black", "black", "red", "blue"), 
           bty = "n", 
           pch = c(1, NA, NA, NA),
           lty = c(NA,1,2,3),
           pt.cex = 1.1, 
           cex = 1.1, 
           text.col = c("black", "black", "red", "blue"), 
           horiz = F )
    
    put.fig.letter('C', offset = c(0.02, -0.01), cex=2, font=2)
    legend("topleft", "Nonlytic Model", adj=0.15, bty = "n", cex = 1.1)
    text(6.1, 2.8, col = "black", bquote(paste('V'[0]*' = 8.24*10'^-6*' TCID'[50]*'/ml')))
    text(6, 4, col = "red", bquote(paste('V'[0]*' = 8.24*10'^-5*' TCID'[50]*'/ml')))
    text(3.0, 0.6, col = "blue", bquote(paste('V'[0]* '= 8.24*10'^-7*' TCID'[50]*'/ml')))
  }
  
  fit<-c(r=1.65E+01, d=2.93E+00, h=6.16E+01, Vs=8.24E-06)
  eval(substitute((h*A0*exp(rho*t))^n,as.list(c(fix,fit,t=2))))
  
  # plotting the data and model solutions given the parameter guesses
  {
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    plot(Viralload[,1],(Viralload[,2]),xlab="Days Since Infection",
         ylab=bquote(paste("Viral Titer")), yaxt = "n",
         ylim=c(0,8), cex.axis=1.2, cex.lab=1.2)
    axis(2, las = 1, at = seq(0, 8, 2), labels = c(expression(paste("10"^0)), expression(paste("10"^2)), expression(paste("10"^4)), expression(paste("10"^6)), expression(paste("10"^8))))
    lines(sol[,1],log10(sol[,2]),type="l",col=1,lwd=2)
    fit<-c(r=1.65E+01, d=2.93E+00, h=6.66E+01, Vs=8.24E-06)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=2,col="red",lwd=2)
    fit<-c(r=1.65E+01, d=2.93E+00, h=5.66E+01, Vs=8.24E-06)
    sol<-ode.solve(fit,fix,virusGrowth,init.vector,seq(0,10,0.02),"Days")
    lines(sol[,1],log10(sol[,2]),lty=3,col="blue",lwd=2)
    
    legend("topright", 
           title = "Saturation Factor Influence",
           legend = c("Flu A ID-33", "Fitted h", "Increased h", "Decreased h"), 
           col = c("black", "black", "red", "blue"), 
           bty = "n", 
           pch = c(1, NA, NA, NA),
           lty = c(NA,1,2,3),
           pt.cex = 1.1, 
           cex = 1.1, 
           text.col = c("black", "black", "red", "blue"), 
           horiz = F )
    
    put.fig.letter('D', offset = c(0.02, -0.01), cex=2, font=2)
    legend("topleft", "Nonlytic Model", adj=0.15, bty = "n", cex = 1.1)
    text(6.5, 1.0, col = "black", expression(paste(h,' = 61.6')))
    text(2.3, 2, col = "red", expression(paste(h,' = 66.6')))
    text(5.8, 4, col = "blue", expression(paste(h,' = 56.6')))
  }
  
  dev.off()
  
  


