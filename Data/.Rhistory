scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
geom_text(aes(x = 5, y = 15, label = "Cytolytic Model"), cex = 3)+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
kcytplot <- ggplot(fluacyt, aes(x = dosage, y = k))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Killing Per Day (k)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 50))
hcytplot <- ggplot(fluacyt, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 70, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 75))
Vcytplot <- ggplot(fluacyt, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 6, label = "Cytolytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#TCL
{
betatclplot <- ggplot(fluatcl, aes(x = dosage, y = beta))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 0.05))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 0.044, label = "Simplified TCL\nModel"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = expression(paste("Viral Infection Per TCID 50/ml Per Day ( ", beta, ")")))
ftclplot <- ggplot(fluatcl, aes(x = dosage, y = f))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Viral Proliferation Per TCID 50/ml (f)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 50, label = "Simplified TCL\nModel"), cex = 3)
dtclplot <- ggplot(fluatcl, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 35, label = "Simplified TCL\nModel"), cex = 3)+
scale_y_continuous(limits = c(0, 40))
Vtclplot <- ggplot(fluatcl, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 7, label = "Simplified TCL\nModel"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#NON
{
rnonplot <- ggplot(fluanon, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 50))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Nonlytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
dnonplot <- ggplot(fluanon, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 28, label = "Nonlytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 30))
hnonplot <- ggplot(fluanon, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 80, label = "Nonlytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 85))
Vnonplot <- ggplot(fluanon, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
geom_text(aes(x = 5, y = 8, label = "Nonlytic Model"), cex = 3)+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
cyt.page <- ggarrange(rcytplot, kcytplot, hcytplot, Vcytplot,
ncol = 2, nrow = 2, legend = "none")
tcl.page <- ggarrange(betatclplot, ftclplot, dtclplot, Vtclplot,
ncol = 2, nrow = 2, legend = "none")
non.page <- ggarrange(rnonplot, dnonplot, hnonplot, Vnonplot,
ncol = 2, nrow = 2, legend = "none")
pdf("Monolix.pdf", width = 11, height = 11)
cyt.page
tcl.page
non.page
dev.off()
#CYT
{
rcytplot <- ggplot(fluacyt, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 16))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
geom_text(aes(x = 5, y = 15, label = "Cytolytic Model"), cex = 3)+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
kcytplot <- ggplot(fluacyt, aes(x = dosage, y = k))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Killing Per Day (k)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 50))
hcytplot <- ggplot(fluacyt, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 70, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 75))
Vcytplot <- ggplot(fluacyt, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 8, label = "Cytolytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#TCL
{
betatclplot <- ggplot(fluatcl, aes(x = dosage, y = beta))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 0.05))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 0.044, label = "Simplified TCL\nModel"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = expression(paste("Viral Infection Per TCID 50/ml Per Day ( ", beta, ")")))
ftclplot <- ggplot(fluatcl, aes(x = dosage, y = f))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Viral Proliferation Per TCID 50/ml (f)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 50, label = "Simplified TCL\nModel"), cex = 3)
dtclplot <- ggplot(fluatcl, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 35, label = "Simplified TCL\nModel"), cex = 3)+
scale_y_continuous(limits = c(0, 40))
Vtclplot <- ggplot(fluatcl, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 8, label = "Simplified TCL\nModel"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#NON
{
rnonplot <- ggplot(fluanon, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 50))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Nonlytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
dnonplot <- ggplot(fluanon, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 28, label = "Nonlytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 30))
hnonplot <- ggplot(fluanon, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 80, label = "Nonlytic Model"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 85))
Vnonplot <- ggplot(fluanon, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
geom_text(aes(x = 5, y = 8, label = "Nonlytic Model"), cex = 3)+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
cyt.page <- ggarrange(rcytplot, kcytplot, hcytplot, Vcytplot,
ncol = 2, nrow = 2, legend = "none")
tcl.page <- ggarrange(betatclplot, ftclplot, dtclplot, Vtclplot,
ncol = 2, nrow = 2, legend = "none")
non.page <- ggarrange(rnonplot, dnonplot, hnonplot, Vnonplot,
ncol = 2, nrow = 2, legend = "none")
pdf("Monolix.pdf", width = 11, height = 11)
cyt.page
tcl.page
non.page
dev.off()
#CYT
{
rcytplot <- ggplot(fluacyt, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 16))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 13, label = paste("(p = ", round(rcyttest$p.value, 2), ")", sep = ""))+
geom_text(aes(x = 5, y = 15, label = "Cytolytic Model"), cex = 3)+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
kcytplot <- ggplot(fluacyt, aes(x = dosage, y = k))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Killing Per Day (k)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 43, label = paste("(p = ", round(kcyttest$p.value, 2), ")", sep = ""))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 50))
hcytplot <- ggplot(fluacyt, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 60, label = paste("(p = ", round(hcyttest$p.value, 2), ")", sep = ""))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 70, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 75))
Vcytplot <- ggplot(fluacyt, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 8, label = "Cytolytic Model"), cex = 3)+
annotate("text", x = 5, y = 7, label = paste("(p = ", round(Vscyttest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#TCL
{
betatclplot <- ggplot(fluatcl, aes(x = dosage, y = beta))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 0.05))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 0.044, label = "Simplified TCL\nModel"), cex = 3)+
annotate("text", x = 5, y = 0.035, label = paste("(p = ", round(betatcltest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = expression(paste("Viral Infection Per TCID 50/ml Per Day ( ", beta, ")")))
ftclplot <- ggplot(fluatcl, aes(x = dosage, y = f))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Viral Proliferation Per TCID 50/ml (f)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 45, label = paste("(p = ", round(ftcltest$p.value, 2), ")", sep = ""))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 50, label = "Simplified TCL\nModel"), cex = 3)
dtclplot <- ggplot(fluatcl, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 33, label = paste("(p = ", round(dtcltest$p.value, 2), ")", sep = ""))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 35, label = "Simplified TCL\nModel"), cex = 3)+
scale_y_continuous(limits = c(0, 40))
Vtclplot <- ggplot(fluatcl, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
annotate("text", x = 5, y = 7, label = paste("(p = ", round(Vstcltest$p.value, 2), ")", sep = ""))+
geom_text(aes(x = 5, y = 8, label = "Simplified TCL\nModel"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#NON
{
rnonplot <- ggplot(fluanon, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 50))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Nonlytic Model"), cex = 3)+
annotate("text", x = 5, y = 45, label = paste("(p = ", round(rnontest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
dnonplot <- ggplot(fluanon, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 28, label = "Nonlytic Model"), cex = 3)+
annotate("text", x = 5, y = 23, label = paste("(p = ", round(dnontest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 30))
hnonplot <- ggplot(fluanon, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 80, label = "Nonlytic Model"), cex = 3)+
annotate("text", x = 5, y = 70, label = paste("(p = ", round(hnontest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 85))
Vnonplot <- ggplot(fluanon, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
geom_text(aes(x = 5, y = 8, label = "Nonlytic Model"), cex = 3)+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
annotate("text", x = 5, y = 7, label = paste("(p = ", round(Vsnontest$p.value, 2), ")", sep = ""))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
cyt.page <- ggarrange(rcytplot, kcytplot, hcytplot, Vcytplot,
ncol = 2, nrow = 2, legend = "none")
tcl.page <- ggarrange(betatclplot, ftclplot, dtclplot, Vtclplot,
ncol = 2, nrow = 2, legend = "none")
non.page <- ggarrange(rnonplot, dnonplot, hnonplot, Vnonplot,
ncol = 2, nrow = 2, legend = "none")
pdf("Monolix.pdf", width = 11, height = 11)
cyt.page
tcl.page
non.page
dev.off()
#CYT
{
rcytplot <- ggplot(fluacyt, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 16))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 13, label = paste("(p = ", round(rcyttest$p.value, 2), ")", sep = ""))+
geom_text(aes(x = 5, y = 15, label = "Cytolytic Model"), cex = 3)+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
kcytplot <- ggplot(fluacyt, aes(x = dosage, y = k))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Killing Per Day (k)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 43, label = paste("(p = ", round(kcyttest$p.value, 4), ")", sep = ""))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 50))
hcytplot <- ggplot(fluacyt, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 60, label = paste("(p = ", round(hcyttest$p.value, 2), ")", sep = ""))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 70, label = "Cytolytic Model"), cex = 3)+
scale_y_continuous(limits = c(0, 75))
Vcytplot <- ggplot(fluacyt, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 8, label = "Cytolytic Model"), cex = 3)+
annotate("text", x = 5, y = 7, label = paste("(p = ", round(Vscyttest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#TCL
{
betatclplot <- ggplot(fluatcl, aes(x = dosage, y = beta))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 0.05))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 0.044, label = "Simplified TCL\nModel"), cex = 3)+
annotate("text", x = 5, y = 0.035, label = paste("(p = ", round(betatcltest$p.value, 4), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = expression(paste("Viral Infection Per TCID 50/ml Per Day ( ", beta, ")")))
ftclplot <- ggplot(fluatcl, aes(x = dosage, y = f))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Viral Proliferation Per TCID 50/ml (f)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 45, label = paste("(p = ", round(ftcltest$p.value, 2), ")", sep = ""))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 50, label = "Simplified TCL\nModel"), cex = 3)
dtclplot <- ggplot(fluatcl, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
annotate("text", x = 5, y = 33, label = paste("(p = ", round(dtcltest$p.value, 2), ")", sep = ""))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 35, label = "Simplified TCL\nModel"), cex = 3)+
scale_y_continuous(limits = c(0, 40))
Vtclplot <- ggplot(fluatcl, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
annotate("text", x = 5, y = 7, label = paste("(p = ", round(Vstcltest$p.value, 2), ")", sep = ""))+
geom_text(aes(x = 5, y = 8, label = "Simplified TCL\nModel"), cex = 3)+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
#NON
{
rnonplot <- ggplot(fluanon, aes(x = dosage, y = r))+
geom_jitter(position=position_jitter(0.2))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
scale_y_continuous(limits = c(0, 50))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
labs(tag ="A") + theme(plot.tag = element_text(face = "bold", size = 25))+
geom_text(aes(x = 5, y = 48, label = "Nonlytic Model"), cex = 3)+
annotate("text", x = 5, y = 45, label = paste("(p = ", round(rnontest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(x="Drug Concentration", y = "Virus Proliferation Per Day (r)")
dnonplot <- ggplot(fluanon, aes(x = dosage, y = d))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = expression(paste("Viral Death Per Day ( ", delta, ")")))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 28, label = "Nonlytic Model"), cex = 3)+
annotate("text", x = 5, y = 23, label = paste("(p = ", round(dnontest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="B") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 30))
hnonplot <- ggplot(fluanon, aes(x = dosage, y = h))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
labs(x="Drug Concentration", y = "Immune Response Saturation Factor (h)")+
geom_hline(yintercept=0, linetype="dashed", color="black")+
geom_text(aes(x = 5, y = 80, label = "Nonlytic Model"), cex = 3)+
annotate("text", x = 5, y = 70, label = paste("(p = ", round(hnontest$p.value, 2), ")", sep = ""))+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="C") + theme(plot.tag = element_text(face = "bold", size = 25))+
scale_y_continuous(limits = c(0, 85))
Vnonplot <- ggplot(fluanon, aes(x = dosage, y = Vv))+
stat_summary(fun=mean, geom="crossbar",  width = 0.5, color="red")+
geom_jitter(position=position_jitter(0.2))+
geom_text(aes(x = 5, y = 8, label = "Nonlytic Model"), cex = 3)+
labs(x="Drug Concentration", y = expression('Initial Viral Amount TCID 50/ml (V'[0]*')'))+
annotate("text", x = 5, y = 7, label = paste("(p = ", round(Vsnontest$p.value, 2), ")", sep = ""))+
geom_hline(yintercept=0, linetype="dashed", color="black")+
scale_x_discrete(limits=c("Placebo", "20 mg", "100 mg", "200 mg", "200 mg od"), labels=c("Placebo\n(n = 12)", "20 mg\n(n = 13)", "100 mg\n(n = 13)", "200 mg\n(n = 15)", "200 mg od\n(n = 13)"))+
labs(tag ="D") + theme(plot.tag = element_text(face = "bold", size = 25))
}
cyt.page <- ggarrange(rcytplot, kcytplot, hcytplot, Vcytplot,
ncol = 2, nrow = 2, legend = "none")
tcl.page <- ggarrange(betatclplot, ftclplot, dtclplot, Vtclplot,
ncol = 2, nrow = 2, legend = "none")
non.page <- ggarrange(rnonplot, dnonplot, hnonplot, Vnonplot,
ncol = 2, nrow = 2, legend = "none")
pdf("Monolix.pdf", width = 11, height = 11)
cyt.page
tcl.page
non.page
dev.off()
